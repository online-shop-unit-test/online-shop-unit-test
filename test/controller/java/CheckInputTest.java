/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package controller.java;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;





/**
 *
 * @author lamnb
 */

public class CheckInputTest {
    
     @Test
    public void isPosIntShouldReturnTrueWhenInputIsPositiveInteger() {
        CheckInput checkInput = new CheckInput();
        assertEquals(true, checkInput.isPosInt("10"));
        assertEquals(true, checkInput.isPosInt("100"));
        assertEquals(true, checkInput.isPosInt("9999"));
    }

    @Test
    public  void isPosIntShouldReturnFalseWhenInputIsNotPositiveInteger() {
        CheckInput checkInput = new CheckInput();
        assertEquals(false, checkInput.isPosInt("-10"));
        assertEquals(false, checkInput.isPosInt("0"));
        assertEquals(false, checkInput.isPosInt("abc"));
    }

    @Test
    public void isPosDoubleShouldReturnTrueWhenInputIsPositiveDouble() {
        CheckInput checkInput = new CheckInput();
        assertEquals(true, checkInput.isPosDouble("10.5"));
        assertEquals(true, checkInput.isPosDouble("3.14"));
        assertEquals(true, checkInput.isPosDouble("9999.99"));
    }

    @Test
    public void isPosDoubleShouldReturnFalseWhenInputIsNotPositiveDouble() {
        CheckInput checkInput = new CheckInput();
        assertEquals(false, checkInput.isPosDouble("-10.5"));
        assertEquals(false, checkInput.isPosDouble("0"));
        assertEquals(false, checkInput.isPosDouble("abc"));
    }
    @Test
    public void isPosInt_ShouldReturnTrue_WhenInputIsPositiveInteger() {
        CheckInput checkInput = new CheckInput();
        assertTrue(checkInput.isPosInt("10"));
        assertTrue(checkInput.isPosInt("100"));
        assertTrue(checkInput.isPosInt("9999"));
    }

    @Test
    public void isPosInt_ShouldReturnFalse_WhenInputIsNotPositiveInteger() {
        CheckInput checkInput = new CheckInput();
        assertFalse(checkInput.isPosInt("-10"));
        assertFalse(checkInput.isPosInt("0"));
        assertFalse(checkInput.isPosInt("abc"));
    }
    
    @Test
    public void isPosInt_ShouldReturnFalse_WhenInputIsOutOfRange() {
        CheckInput checkInput = new CheckInput();
        assertFalse(checkInput.isPosInt("2147483648")); // Integer.MAX_VALUE + 1
        assertFalse(checkInput.isPosInt("-2147483649")); // Integer.MIN_VALUE - 1
    }

    @Test
    public void isPosDouble_ShouldReturnTrue_WhenInputIsPositiveDouble() {
        CheckInput checkInput = new CheckInput();
        assertTrue(checkInput.isPosDouble("10.5"));
        assertTrue(checkInput.isPosDouble("3.14"));
        assertTrue(checkInput.isPosDouble("9999.99"));
    }

    @Test
    public void isPosDouble_ShouldReturnFalse_WhenInputIsNotPositiveDouble() {
        CheckInput checkInput = new CheckInput();
        assertFalse(checkInput.isPosDouble("-10.5"));
        assertFalse(checkInput.isPosDouble("0"));
        assertFalse(checkInput.isPosDouble("abc"));
    }
    
    

    
}
