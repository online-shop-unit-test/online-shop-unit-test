/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package controller.crudp;

import dal.ProductDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author admin
 */
public class ListProductServletTest {
    @Mock
     HttpServletRequest request;

    @Mock
     HttpServletResponse response;

//    @Mock
//    private ProductDAO productDAO;

    @Mock
     RequestDispatcher requestDispatcher;

     ListProductServlet servlet;
    
    
    @Before
    public void setUp() {
        
        servlet = new ListProductServlet();
    }
    public ListProductServletTest() {
    }

    /**
     * Test of doGet method, of class ListProductServlet.
     */
    @Test
    public void testDoGet() throws Exception {
        System.out.println("doGet");
        ListProductServlet instance = new ListProductServlet();
        if(request == null){
            System.out.println("error");
        }
        instance.doGet(request, response);
        fail("The test case is a prototype.");
    }

    /**
     * Test of doPost method, of class ListProductServlet.
     */
    @Test
    public void testDoPost() throws Exception {
        System.out.println("doPost");
        HttpServletRequest request = null;
        HttpServletResponse response = null;
        ListProductServlet instance = new ListProductServlet();
        instance.doPost(request, response);
        fail("The test case is a prototype.");
    }
    
}
