package controller.customer;

import controller.java.CookieCart;
import dal.CustomerDAO;
import dal.OrderDAO;
import dal.OrderDetailsDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.Customer;
import model.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)

public class CartServletTest {

    @Mock
    HttpServletRequest request;

    @Mock
    HttpServletResponse response;

    @Mock
    HttpSession session;

    @Mock
    Account account;

    @Mock
    CustomerDAO customerDAO;

    @Mock
    Customer customer;

    Cookie[] cookies = {new Cookie("cart", "404:3/405:5/406:2")};;

    @Mock
    CookieCart cookieCart;

    @Mock
    OrderDAO orderDAO;

    @Mock
    OrderDetailsDAO orderDetailsDAO;

    @Mock
    List<Product> productList;
    
    @Mock
    private RequestDispatcher dispatcher;

    @InjectMocks
    CartServlet cartServlet;

    // Test doGet() method to check cart items are retrieved correctly when the user is logged in
    // Get the cookie with the given name
    private Cookie getCookieByName(Cookie[] cookies, String name) {
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (name.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }
        return null;
    }

// Test doGet() method to check cart items are retrieved correctly when the user is logged in
    @Test
    public void testDoGetWithLoggedInUser() throws Exception { 
        // Setup
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher(anyString())).thenReturn(dispatcher);
        when(request.getCookies()).thenReturn(cookies);
        Cookie cookie = getCookieByName(cookies, "cart");
//        when(cookie.getValue()).thenReturn("404:3/405:5/406:2");
        when(session.getAttribute("account")).thenReturn(account);
        when(account.getUserName()).thenReturn("testuser");
        when(customerDAO.getByUserName("testuser")).thenReturn(customer);
        when(customer.getFullName()).thenReturn("Test User");
        when(customer.getPhone()).thenReturn("1234567890");
        when(customer.getAddress()).thenReturn("123 Test Street");
        when(cookieCart.getListcart(cookie.getValue())).thenReturn(productList);
//        request.setAttribute("cu", customer);
//        request.setAttribute("listCart", productList);
        
//        // Execution
        cartServlet.doGet(request, response);
//
//        // Verification
        assertEquals("Test User", customer.getFullName());
        assertEquals("1234567890", customer.getPhone());
        assertEquals("123 Test Street", customer.getAddress());
    }

    // Test doGet() method to check cart items are retrieved correctly when the user is not logged in
//    @Test
//    public void testDoGetWithNotLoggedInUser() throws Exception {
//        MockitoAnnotations.initMocks(this);
//
//        // Setup
//        when(request.getCookies()).thenReturn(cookies);
//        when(cookies.getName()).thenReturn("cart");
//        when(cookies.getValue()).thenReturn("productId:1|quantity:2");
//        when(session.getAttribute("account")).thenReturn(null);
//        when(cookieCart.getListcart(cookies[0].getValue())).thenReturn(productList);
//        request.setAttribute("listCart", productList);
//
//        // Execution
//        cartServlet.doGet(request, response);
//
//        // Verification
//        assertEquals(productList, request.getAttribute("listCart"));
//    }
//
//    // Test doPost() method to check customer information is updated correctly and order details are added to database
//    @Test
//    public void testDoPost() throws Exception {
//        MockitoAnnotations.initMocks(this);
//
//        // Setup
//        when(session.getAttribute("account")).thenReturn(account);
//        when(account == null).thenReturn(false);
//        when(request.getParameter("fullName")).thenReturn("Test User");
//        when(request.getParameter("phone")).thenReturn("1234567890");
//        when(request.getParameter("address")).thenReturn("123 Test Street");
//        when(customerDAO.getByUserName(account.getUserName())).thenReturn(customer);
//        when(orderDAO.getNewestOrder()).thenReturn(1);
//        List<Product> listCart = new ArrayList<Product>();
//        Product product = new Product();
//        product.setProductId(1);
//        product.setQuantity(2);
//        listCart.add(product);
//        when(cookieCart.getListcart("productId:1|quantity:2")).thenReturn(listCart);
//
//        // Execution
//        cartServlet.doPost(request, response);
//
//        // Verification
//        AssassertEquals("Test User", customer.getFullName());
//        assertEquals("1234567890", customer.getPhone());
//        assertEquals("123 Test Street", customer.getAddress());
//        OrderDetails orderDetail = new OrderDetails();
//        orderDetail.setOrderId(1);
//        orderDetail.setProductId(1);
//        orderDetail.setQuatity(2);
//        List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();
//        orderDetailsList.add(orderDetail);
//        when(orderDetailsDAO.getByOrderId(1)).thenReturn(orderDetailsList);
//    }
//
//// Test doPost() method to check customer information is not updated and order details are not added to database when the user is not logged in
//    @Test
//    public void testDoPostWithNotLoggedInUser() throws Exception {
//        MockitoAnnotations.initMocks(this);
//
//        // Setup
//        when(session.getAttribute("account")).thenReturn(null);
//        List<Product> listCart = new ArrayList<Product>();
//        Product product = new Product();
//        product.setProductId(1);
//        product.setQuantity(2);
//        listCart.add(product);
//        when(cookieCart.getListcart("productId:1|quantity:2")).thenReturn(listCart);
//
//        // Execution
//        cartServlet.doPost(request, response);
//
//        // Verification
//        assertEquals(null, session.getAttribute("customer"));
//        assertEquals(null, session.getAttribute("orderId"));
//        assertEquals(null, session.getAttribute("listCart"));
//    }
//
//// Test doPost() method to check cart items are removed from cookie correctly after placing order and session is invalidated 
//    @Test
//    public void testDoPostWithPlacingOrder() throws Exception {
//        MockitoAnnotations.initMocks(this);
//
//        // Setup
//        when(session.getAttribute("account")).thenReturn(account);
//        when(account == null).thenReturn(false);
//        when(request.getParameter("fullName")).thenReturn("Test User");
//        when(request.getParameter("phone")).thenReturn("1234567890");
//        when(request.getParameter("address")).thenReturn("123 Test Street");
//        when(customerDAO.getByUserName(account.getUserName())).thenReturn(customer);
//        when(orderDAO.getNewestOrder()).thenReturn(1);
//        List<Product> listCart = new ArrayList<Product>();
//        Product product = new Product();
//        product.setProductId(1);
//        product.setQuantity(2);
//        listCart.add(product);
//        when(cookieCart.getListcart("productId:1|quantity:2")).thenReturn(listCart);
//
//        // Execution
//        cartServlet.doPost(request, response);
//
//        // Verification
//        assertEquals(null, cookies[0].getValue());
//        assertEquals(null, session.getAttribute("customer"));
//        assertEquals(null, session.getAttribute("orderId"));
//        assertEquals(null, session.getAttribute("listCart"));
//
//    }
}
